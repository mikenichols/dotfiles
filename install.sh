#!/bin/zsh

export DOTFILES_DIR="${0:a:h}"
echo "using $DOTFILES_DIR"
cp -rf .zsh_functions $HOME
zsh $HOME/.zsh_functions/provision "$DOTFILES_DIR"

nohup alacritty>/dev/null&




# Required in OSX High Sierra and newer
export OBJC_DISABLE_INITIALIZE_FORK_SAFETY=YES

export LANG=en_US.UTF-8
#Use the "where" command
ASDF_BIN=/opt/homebrew/bin/asdf
GOV=$($ASDF_BIN where golang)
export GOROOT=$GOV/go

# flutter support
export PATH="$PATH:$HOME/AndroidStudioProjects/flutter/bin"

# Apple Silicon machines dont add brew to path
if [ -d /opt/homebrew/bin ]; then
	eval "$(/opt/homebrew/bin/brew shellenv)"
fi
